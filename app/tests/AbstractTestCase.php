<?php

namespace SH\App\Tests;

use PHPUnit\Framework\TestCase;

abstract class AbstractTestCase extends TestCase {

    public function setUp(): void
    {
        parent::setUp();
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

}
