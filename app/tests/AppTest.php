<?php

namespace SH\App\Tests;

use SH\App\App;
use SH\Package\Service;

class AppTest extends AbstractTestCase {

    public function testNameCanBeSetAndRetrieved() {
        $app = new App(new Service());
        $app->setName('John Doe');

        $this->assertSame('John Doe', $app->getName());
    }

}
