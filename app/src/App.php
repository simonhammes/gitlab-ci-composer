<?php

namespace SH\App;

use SH\Package\Service;

class App {

    protected Service $service;

    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    public function setName(string $name): void
    {
        $this->service->setName($name);
    }

    public function getName(): string
    {
        return $this->service->getName();
    }

}
